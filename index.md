> 1/2 page = 1300 signes environ
> 1 page = 2600 signes environ

—--------------------—

cheminement méta

intro > agir dans sa relation au public (1 à 4) > agir pour et sur soi (5 à 8)> agir dans et sur le système (9 à 12)> conclusion

—--------------------—


## sommaire article

### [page 1](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page1.md)
T1 Introduction

T2 / 1-présentation de l'asso PiNG 

  > 1/2 page

T2 / 2-d'où je parle pour le monde de la bibliothèque + problématique / questionnements et enjeux travail posture présentation démarche inventaire
  > 1/2 page

### [page 2](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page2.md) 
T1 Inventaire à la Prévert

T2 - 1 - accueillir, écouter et questionner avec bienveillance plutôt que d'anticiper ses besoins et lui donner nos propres réponses

  > 1/2 page 

T2 - 2 - stimuler la curiosité / apprendre à pécher plutôt que de donner le poisson
  > 1/2 page
  
### [page 3](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page3.md)
T2 - 3 - mettre en situation plutôt que faire à la place de

  > 1/2 page

T2 - 4 - favoriser l'apprentissage et l'entraide entre pairs
  > 1/2 page

### [page 4](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page4.md)

T2 - 5 - encourager et mettre en oeuvre des attitudes, processus et pratiques réflexives, critiques - mettre sa démarche réflexive en mouvement

  > 1/2 page
 
T2 -6 - Adapter sa stratégie, ses méthodes, ses outils pédagogiques aux besoins exprimés par les publics / personnes concernées
  > 1/2 page


### [page 5](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page5.md)

T2 - 7 - développer une conscience critique de son pouvoir
  > 1/2 page
  
T2 - 8 - se former tout au long de sa vie
  > 1/2 page



### [page 6](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page6.md)


T2 - 9 - mettre en oeuvre un panel d'activité informelles à plus formelles / créer son propre parcours d"apprentissage
  > 1/2 page
 
T2 - 10 - faire évoluer l'aménagement du lieu vers toujours plus d'accessibilité, d'ergonomie des usages et des outils
  > 1/2 page


### [page 7](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page7.md)

T2 - 11 - déconstruire schémas / représentation sur les pubics > curriculum
  > 1/2 page
  
T2 - 12 - Participer à la construction d’écosystèmes ouverts de reconnaissance  des apprentissages 
  > 1/2 page

### [page 8](/home/pclf/Nextcloud/ESSAIMER/ARTICLES/ENSSIB/redac/page8.md)
 T1 En conclusion

> élargissement vers démarche recherche-action / pédagogies de transformation sociale / pédagogies sociales / collectifs territoriaux transversaux (type badgeons les Pays de la Loire)

>  1 page

—--------------------—


## résumé article V2 03/03/20

La  tâche d’une personne en charge de l’accueil et de l’animation d'un atelier ne  consiste pas à transférer du savoir, mais à créer la possibilité de  production ou de construction de ce savoir.
L’objectif est de favoriser le développement de l’autonomie et l’émancipation des personnes (dans le sens « savoir agir », « vouloir  agir » et « pouvoir d’agir » ou "puissance d'agir"), dans une perspective plus large de  transformation sociale de la société à l’ère numérique. Dans  cette perspective, Maëlle Vimont a commencé à cheminer elle-même sur ces questions en lien avec sa pratique professionnelle au sein de l'écosystème des tiers-lieux, fablabs, lieux d'appropriation des cultures numériques. Elle partage dans cette article un inventaire “à la Prévert” - résolument non exhaustif et subjectif - d’actions concrètes, réflexions et attitudes adoptées ou à développer, pour aller vers la construction d'une éthique de l'accueil et de l'animation d’ateliers en tous genres.