# Inventaire à la Prévert
## - 1 - accueillir, écouter et questionner avec bienveillance plutôt que d'anticiper les besoins et donner ses propres réponses

  > 1/2 page / 1300 signes
  
  :::memo
  attitudes, actions concrètes, réflexions
  :::
  
  Comment démarrer cet inventaire ?
  Peut-être par le commencement. Quand on anime un atelier, la première chose qu'on fait (mis à part la préparation!), c'est d'accueillir la ou les personnes qui se présentent à l'atelier qu'on anime. Ces personnes sont peut-être déjà inscrites, ou se présentent spontanément. Peut-être que je les connais déjà, peut-être que c'est la première fois qu'on se rencontrera.
  
  Comment accueillir? 

- **avant l'atelier** :
- préparer du café, du thé, du jus de fruits, quelques trucs à grignoter si possible
- préparer son espace, les supports, tout ce qu'il faut pour être disponible quand les personnes arriveront
 - un peu avant le démarrage de l'atelier, ouvrir la porte, et la laisser ouverte, pour que la/les personnes puissent entrer, sans avoir à frapper.
  - si possible, prévoir un affichage pour guider les personnes jusqu'au lieu de l'atelier. 
  
  - **à l'heure du rendez-vous**
  - se tenir prêt·e pour accueillir les personnes. Autrement dit, guetter la porte !
  - Quand une personne, ou plusieurs, arrivent: inviter à entrer, souhaiter la bienvenue, se présenter ("je suis Maëlle, c'est moi qui vais animer l'atelier". 
  - Demander, si je ne connais pas la / les personnes, "quel est votre / ton prénom?". Demander, selon le contexte et le cadre que vous souhaitez poser dans l'atelier, "est-ce que je peux vous tutoyer ?" ou version plus informelle: "on se tutoie ?"
  - Si je ne sais pas quel pronom utiliser pour m'adresser à la personne (elle? il ? iel? ...), lui poser la question : "Quel pronom souhaites-tu que j'utilise ?"
  - Indiquer rapidement quelques infos clés pour permettre à la personne de prendre ses repères de base : infos sur le lieu où on se trouve (on est dans l'espace de fabrication, c'est ici que l'atelier va se passer...), les toilettes sont là, tu peux poser tes affaires ici…
  - Proposer rapidement : est-ce que tu veux un café , un thé, un verre d'eau ?
  - Personnellement, j'aime bien dire à la personne "fais comme chez toi"
  - Questions bienveillantes en vrac au moment de l'arrivée pour entamer le dialogue : est-ce que tu as trouvé facilement ? C'est la première fois que tu viens ? ...
  
  J'essaye de prendre le temps au maximum d'accueillir individuellement chaque personne, et d'installer un cadre de confiance et convivial. J'essaye de me montrer souriante, et d'adopter une attitude confiante et ouverte. Au fur et à mesure des arrivées des personnes, je présente les participant·es au un·es et aux autre·s. Si je trouve des points communs potentiels entre des participant·es à ce stade, je leur indique, pour les inviter mine de rien à commencer à discuter ensemble avant le démarrage de l'atelier.
  Par exemple : 
  - c'est la première fois que tu viens ? 
  - oui
  - Bienvenue ! Il se trouve que c'est la première fois aussi que XXXXX vient ici. N'hésitez pas si vous avez des questions, je suis là.

**Entre le moment où les gens arrivent et le moment où l'atelier démarre effectivement**:
au fablab, à l'atelier partagé, ou ailleurs, j'essaye toujours de faire un coin café, comme une zone d'accueil, où je pourrai échanger de façon informelle avec les personnes avec un café, et où les personnes peuvent échanger ensemble pendant que j'accueille les autres.
Au bout d'un moment, j'indique généralement "je règle quelques petits détails et on va pouvoir démarrer dans quelques minutes". "Quand vous êtes prêt·es, vous pouvez venir vous installer ici". C'est le moment où certain·es choisissent de faire un tour aux toilettes avant de démarrer, ou de se resservir un café, puis les personnes viennent s'installer d'elles-mêmes à l'endroit où l'atelier démarre. Symboliquement, je pense que c'est un détail important, car les personnes restent "maître·sse" de leur déplacementr. Quand elles s'installent, elles sont de fait prêt·es à démarrer, j'ai toute leur attention.



**tout au long de l'atelier**
à chaque fois que c'est possible, et notamment si la personne me questionne (ex: "comment faire ça?"), j'essaye, plutôt que d'expliquer direct, de répondre par une autre question : "comment tu ferais toi?", "comment tu imagines comment on peut faire ça ?" … j'essaye de faire en sorte que ce soit la personne qui formule les réponses, et que je ne sois là que pour guider la formulation de ces réponses. Cela permet à la personne de construire son cheminement de pensée et être dans une situation active. Je m'appuie sur ce cheminement pour lui proposer des voies pour parvenir à l'objectif en vue ("savoir faire ça"), et l'accompagner sur le chemin.

- en cas de souci "je n'arrive pas à faire ça"…
- exemple : "je n'arrive pa	s à dessiner sur ce logiciel"
- - 1 - reconnaître / accueillir la difficulté rencontrée par la personne en décrivant la situation plutôt que de pointer la difficulté sur la personne elle-même: "oui je vois qu'il n'y a pas encore de dessin sur le logiciel" plutôt que "zut, tu n'y arrive pas c'est un  problème". 
- Faire du problème rencontré par la personne votre problème commun. Faire équipe avec la personne. "On va chercher / trouver une solution". Elle n'est pas face toute seule au souci, vous êtes à ces côtés pour l'aider à trouver une solution.

Pour résoudre le problème, questionner d'abord...aider ensuite, en guidant plutôt qu'en faisant à la place de la personne.
  
  
     
  
  
  
  

## - 2 - stimuler la curiosité / apprendre à pécher plutôt que de donner le poisson
  > 1/2 page
  
  
- pour résoudre des problèmes
- pour étendre le champ des possibles  
  
  
  
  
  
  
  
  
  
  
  
  