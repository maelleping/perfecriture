# Introduction
> présentation de l'asso PiNG / ½ page
>
> ##  1- L'association PiNG

**Comment le numérique transforme-t-il notre société ? Quel est son impact sur notre environnement social, technologique, artistique, naturel ? PiNG invite les citoyen·nes et les professionnel·les à des temps de découverte, de pratique et d’échange pour se ré-approprier ensemble les technologies qui nous entourent.**

> détailler



## 2- La démarche
> d'où je parle pour le monde de la bibliothèque + problématique / questionnements et enjeux travail posture présentation démarche inventaire / ½ page
> 12 items, 1 par mois ?


- d'où je parle:
- de mon bureau à la maison, et non plus dans le fourmillement des ateliers.
- j'ai animé le fablab Plateforme C pendant plusieurs années (euh combien déjà?). C'est de cette expérience là, et de celle qui s'en suit et se poursuit, que je vais parler dans cet article.

Pourquoi en parler?
Parce que je pense avoir beaucoup cheminé depuis plusieurs années à ce sujet, et que j'en arrive à une étape de ma pratique (pédagogique, d'animation, d'accueil) où j'essaye d'adopter de plus en plus, de mieux en mieux (je l'espère), une démarche réflexive. Et cette démarche me nourrit et accompagne mes apprentissages, qui se poursuivent, et se poursuivront - c'est un chemin- .

Problématique / Questionnements / enjeux…
Quand j'entre dans un lieu, quelqu'il soit, je prête attention à de multiples détails.
Comment l'espace est organisé, est-ce qu'il me semble accueillant ? Suis-je accueillie ? Par qui, comment ? 
Je viens dans cet espace pour y *faire* quelque chose. Dans le cas d'un atelier de fabrication, j'y viens peut-être avec de la curiosité, pour y réaliser quelque chose de précis ou avec beaucoup d'interrogations. Dans une bibliothèque, cela me semble être la même chose.
Pourquoi je vous dit ça ? Parce qu'il me semble que ces détails auxquels je prête attention (la liste dressée n'est pas exhaustive!), d'autres personnes ont les mêmes réflexes. Plus ou moins consciemment, il me semble que quand nous entrons quelque part, nous y entrons avec notre histoire, nos envies, nos peurs, nos représentations, nos réflexes (tout ça plus ou moins consciemment).

En tant qu'animatrice, en tant que pédagogue, il me semble important de prendre ça en compte, car, comme les personnes qui entrent au sein du lieu, de l'espace, le temps que j'anime - moi aussi je suis amenéeê à être *à leur place* dans d'autres contextes. Nous sommes dès lors - égaux - .  Et je souhaite accueillir, animer; comme j'aimerais, je crois, qu'on m'accueille, et qu'on m'accompagne dans ce que je suis venu faire dans cet espace, ce temps. Que ce soit pour y fabriquer un objet ou pour y parcourir des livres.

Que vient faire l'éthique dans tout ça ?
Je vais commencer par en re-chercher la définition.
https://www.cnrtl.fr/lexicographie/ethique
https://fr.wikipedia.org/wiki/%C3%89thique
https://fr.wikipedia.org/wiki/R%C3%A8gle_d%27or

http://www.gefers.fr/uploads/orga/2009-l_ethique_dans_la_relation_pedagogique-mtw.pdf

https://www.meirieu.com/OUTILSDEFORMATION/ETHIQUE_ET_PEDAGOGIE.pptx.pdf



https://iresmo.jimdofree.com/2019/02/10/une-p%C3%A9dagogie-socio-ethique/

https://www.babelio.com/livres/Freire-Pedagogie-de-lautonomie--Savoirs-necessaires-a-l/88117



