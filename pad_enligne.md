`expérimentation` `écriture` `stream` 
:::success
:mega: Ce pad a été créé le 05 05 2020 par Maëlle de l'équipe de l'[association PiNG](https://info.pingbase.net/) dans le cadre du projet d'écriture d'un article pour les presses de l'ENSSIB et pour nourrir, *en même temps*, les démarches d'exploration `pédagogiques`,`labo commun`, `chronotopies`.
:point_right: Plus d'infos sur PiNG et nos activités: *[pingbase.net](https://www.pingbase.net/)*
:email: Contact: vous pouvez écrire plus bas dans le pad ou envoyer un mail à maelle(at)pingbase.net
:::

# expérimenter un cadre performatif pour écrire > *un article*
---

## la scène
### stream live d'écriture


<iframe src="https://player.twitch.tv/?channel=maellevim" frameborder="0" allowfullscreen="true" scrolling="no" height="378" width="620"></iframe><a href="https://www.twitch.tv/maellevim?tt_content=text_link&tt_medium=live_embed" style="padding:2px 0px 4px; display:block; width:345px; font-weight:normal; font-size:10px; text-decoration:underline;">Regardez une vidéo live de maellevim sur www.twitch.tv</a>


### pad
Si vous le souhaitez, vous pouvez laisser quelques mots ci-dessous sur ce que cette initiative vous inspire (ou pas) et/ou bavarder dans le chat ("clavardage") en bas à droite. Je serai ravie de vous lire et d'échanger avec vous (quand je n'écris pas).
<iframe name="embed_readwrite" src="https://pad.colibris-outilslibres.org/p/perfecrire_ping?showControls=true&showChat=true&showLineNumbers=true&useMonospaceFont=false" width=600 height=400></iframe>



### le résumé de l'article à écrire

"La  tâche d’une personne en charge de l’accueil et de l’animation d'un atelier ne  consiste pas à transférer du savoir, mais à créer la possibilité de  production ou de construction de ce savoir.
L’objectif est de favoriser le développement de l’autonomie et l’émancipation des personnes (dans le sens « savoir agir », « vouloir  agir » et « pouvoir d’agir » ou "puissance d'agir"), dans une perspective plus large de  transformation sociale de la société à l’ère numérique. 


Dans  cette perspective, Maëlle Vimont a commencé à cheminer elle-même sur ces questions en lien avec sa pratique professionnelle au sein de l'écosystème des tiers-lieux, fablabs, lieux d'appropriation des cultures numériques. 
Elle partage dans cette article un inventaire “à la Prévert” - résolument non exhaustif et subjectif - d’actions concrètes, réflexions et attitudes adoptées ou à développer, pour aller vers la construction d'une éthique de l'accueil et de l'animation d’ateliers en tous genres."


### voir l'évolution du texte au fil du temps
J'écris le texte dans plusieurs fichiers, qui se remplissent de mots au fur et à mesure. 
Pour porter un regard sur la construction du texte au fil des sessions d'écriture, j'enregistrerai pas à pas (et au minimum à chaque fin de session) les modifications apportées aux fichiers, sur un répertoire ouvert en ligne sur gitlab.
C'est par ici pour y accéder: https://gitlab.com/maelleping/perfecriture

### mémoire des sessions d'écriture passées

#### **6 mai - 22h45 > 23h30**
**première session - en écoute: [Flavien Berger, radio contre temps](https://www.youtube.com/watch?v=DPNaqjp-F80) .
(:warning: qualité vidéo pourrie désolée - je tenterai d'améliorer ça pour les prochaines fois!**)

- extrait #1 
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/66a2fca6-6c86-4814-b5b6-27f54075ae90" frameborder="0" allowfullscreen></iframe>

> lancement de la 1ère session. Le choix de la musique n'est pas anodin. Les paroles du début, et la démarche plus large de l'artiste avec cet album, entrent en résonnance avec ma démarche. C'est vite apparu évident qu'il fallait que je commence l'écriture en l'écoutant.
---

- extrait #2
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/388656b8-978b-49ff-835a-5279e5214575" frameborder="0" allowfullscreen></iframe>

> où le rythme de la musique semble se confondre avec le rythme d'écriture sur le clavier (dont le son n'est pas enregistré je précise !).
---
- extrait #3
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/6552c471-c5c3-4e1e-a4be-77971ee26f7f" frameborder="0" allowfullscreen></iframe>

> approfondissement de la recherche sur le mot éthique pour préciser ma pensée. Trouverez-vous le détail qui résonne entre 2 parties de l'image ? 
---

:::info
:::

## les coulisses

### comment j'organise tout ça

#### un espace pour écrire

![](/uploads/upload_f0e658c4adfc7633cb3cc8104d7b52a0.JPG)

#### un "protocole" qui se déroule dans le temps

*avant avant*
- je planifie, comme je peux, et plus ou moins en avance en fonction des contraintes de chaque journée, une session d'écriture.
- je partage l'info : heure de début et lien de ce pad
- je prépare du café

*juste avant*
- je me sers un café
- je m'asseois au bureau
- je mets un casque
- je démarre la musique
- je démarre les logiciels nécessaires
- je fais un test rapide de stream et enregistrement

*à l'heure dite*
- je démarre le stream
- je bois une gorgée de café
- je commence à écrire

*quand je m'arrête d'écrire l'article*
- j'enregistre en local
- je mets à jour la version sur gitlab
- je réponds, s'il y a des messages dans le chat et si j'ai le temps
- je mets en pause ou je stoppe le stream et l'enregistrement
- je ferme les logiciels
- je planifie, si je peux, la prochaine session

*plus tard*
- je reprends le fichier de l'enregistrement pour en extraire un bout à mettre ici.
- je réfléchis et j'essaye de poser quelques mots par ici sur la/es  sessions d'écriture passée(s), dans une démarche réflexive.

#### côté technique

- 1 ordinateur sous linux 
- des logiciels installés en local:
    - ++[zettlr](https://www.zettlr.com/)++ pour écrire en markdown un texte long^[*zettlr*: à propos de ce logiciel, une lecture intéressante: Billet de Christophe Masutti sur statium blog [Zettlr : markdown puissance dix](https://golb.statium.link/post/20200218zettlr/)]
    - ++[openbroadcaster software "OBS"](https://obsproject.com/fr/)++ pour la création de la scène de live et l'enregistrement du live stream.
    - ++[git](https://git-scm.com/)++ pour enregistrer les modifications et gérer les versions du texte

- des services en ligne:
    - ++[gitlab](https://gitlab.com/)++> répertoire en ligne de gestion de versions, pour partagee des versions du texte en cours de route
    - à défaut d'une alternative rapide à mettre en place en version libre (*compte tenu du temps dispo et de mes compétences actuelles*) comme [Openstreamingplateform](https://wiki.openstreamingplatform.com/) , et en attendant de prochains développements en ce sens sur peertube ^[à propos du dév de streaming sur peertube consulter, cette page sur [framacolibri.org](https://framacolibri.org/t/pathway-to-live-streaming-and-related-features/7058) , et [celle-ci sur github](https://github.com/Chocobozzz/PeerTube/issues/151#issuecomment-565325535)], j'utilise ++[twitch](https://www.twitch.tv/)++ pour générer et un stream live
    - ++[pad.numerique-en-commun.fr](https://pad.numerique-en-commun.fr/)++ = le service qui m'a permis de créer la page où vous vous trouvez actuellement. J'ai intégré un "iframe" de twitch pour pouvoir voir le stream et chatter ici, sans aller sur le site twitch.


#### inventaire à la prévert (là aussi) de motivations que j'identifie pour écrire de cette façon
- pour me motiver à écrire en me créant un cadre stimulant et créatif...plutôt que subi et confiné...
- pour essayer de focaliser davantage sur le chemin plutôt que sur la destination
- pour tenter de lâcher un peu plus prise sur les ~~ratures~~, retours en arrières, suppressions, (re)formulation d'une pensée en construction permanente (...et donc parfois difficile à mettre en mots pour moi), de laisser faire, assumer le tâtonnement et (me) donner à voir ce processus.
- pour tester, pratiquer des outils, un protocole et pouvoir échanger là dessus avec d'autres
- pour imaginer d'autres formes de transmission, d'échanges, de pratique ouverte et partagée..en distanciel synchrone / asynchrone
- pour nourrir ma démarche réflexive (et notamment comment je construis mon discours)
- pour (l')ouvrir avant que le texte produit soit fermé (pour un temps limité) dans une publication papier et numérique commercialisée, pour laquelle il y aura une re-mise à disposition des textes en ligne sous licence libre quelques mois (années?) après la 1ère parution.
- pour (ré)activer et pratiquer des apprentissages passés : audiovisuel, formation "écriture créative au service de l'information" d'[Amélie Charcosset](http://www.ameliecharcosset.com/bienvenue/) ...et apprendre/pratiquer de nouvelles choses (process streaming, intégrations, versioning, écriture réflexive...)
- pour essayer d'aller vers, humblement, "la spontanéité entre nos doigts et nos mots" au sens de [l'article d'Alexandre Korber, Libération de l'écriture](http://www.rebonds.net/30techthepower/551-liberationdelecriture) et des pratiques expérimentées dans des cadres de recherche-action, au sens de ce numéro des cahiers de l'injep ["écriture réflexive: la pratique innovantes des espaces comme levier de transformation sociale](https://injep.fr/publication/recherche-action-et-ecriture-reflexive-la-pratique-innovante-des-espaces-comme-levier-de-transformation-sociale/).

## Licence de cette page
Ce document est régi par les termes de la licence [Creative Commons CC-BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/) 

<img style="display: block; margin: 0 auto;" src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" width="40%">


#### notes de bas de pad

