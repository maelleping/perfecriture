## - 3 - mettre en situation plutôt que faire à la place de

  > 1/2 page
  
  - si l'activité se déroule sur un ordinateur, ne pas prendre la place de la personne, la guider

## - 4 - favoriser l'apprentissage et l'entraide entre pairs
  > 1/2 page